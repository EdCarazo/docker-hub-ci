FROM ubuntu:latest

RUN apt-get update -y && apt-get install unzip -y && apt-get install wget -y && apt-get install vim -y && apt-get -y install curl

##AWS CLI
RUN apt-get install awscli -y
RUN aws --version

## Terraform
RUN wget https://releases.hashicorp.com/terraform/0.12.21/terraform_0.12.21_linux_amd64.zip
RUN unzip terraform_0.12.21_linux_amd64.zip
RUN mv terraform /usr/local/bin
RUN terraform --version

CMD ["suppervisord","-n"]

